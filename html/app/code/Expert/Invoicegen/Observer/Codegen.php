<?php
 
namespace Expert\Invoicegen\Observer;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Event\ObserverInterface;
                 
 
/**
 * Customer login observer
 */
class Codegen implements ObserverInterface
{
 
    /**   
     * Message manager
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;
 
    /**
     * Constructor
     *
     * @param  \Magento\Framework\Message\ManagerInterface $messageManager Message Manager
     *
     * @return void
     */
   protected $storeManager;
protected $_transportBuilder;
protected $inlineTranslation;
public function __construct(StoreManagerInterface $smi,TransportBuilder $tb,StateInterface $si)
{        
    $this->storeManager=$smi;
    $this->_transportBuilder=$tb;
    $this->inlineTranslation=$si;
}
 
    /**
     * Display a custom message when customer log in
     *
     * @param  \Magento\Framework\Event\Observer $observer Observer
     *
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		
		  
			
		 $invoice = $observer->getEvent()->getInvoice();
		 $order = $invoice->getOrder();
	  $orderid=$order->getIncrementId();
	
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$addressObj = $order->getBillingAddress();
		
		$email= $addressObj->getEmail();
		$customername=$addressObj->getFirstName();
		$message="";
		  $rowCount = 0;
	
	$pArray=array();
               
		foreach ($order->getAllItems() as $item) {
			  
			   $name = $item->getName();
			$type = $item->getSku();
	
			 // if ($rowCount++ % 2 == 1 ) { $type = $item->getSku(); 
			
		
					$productid=$item->getProductId();
				
							$codeforcheck=explode("-",$type);
						$checkformagic = 	$codeforcheck[count($codeforcheck)-2];
				   $skuoftocompare=end($codeforcheck);
				 $myArray=array();
			
				  if($skuoftocompare=="digitaal" || $skuoftocompare=="combi" ){
					  
							  $qty= $item->getData('qty_ordered');
						 
						for($i=1;$i<=$qty;$i++){
							
							$d=date ("d");
							$m=date ("m");
							$y=date ("Y");
							$t=time();
							$dmt=$d+$m+$y+$t;    
							$ran= rand(0,10000000);
							$dmtran= $dmt+$ran;
							$un=  uniqid();
							$dmtun = $dmt.$un;
							$mdun = md5($dmtran.$un);
							$sort=substr($mdun, 16);
							
							$myArray[] =  array($i=>$mdun);
						
						}
					
				$pArray[]=array($item->getSku()=>$myArray);	
						

			}
			
			}
		
			 	
		//}
		
//	$x=1;
		foreach ($pArray as $values) {
	
			foreach($values as $qwer=>$v)
					{
						
						
						$y=1;
						$message.=" ";   
						$correctname=str_replace('-', ' ', $qwer);
	                    $message.=ucwords($correctname);
	                    $message.="<ul>";
				     foreach($v as $v1)
					{
						
						
				
				
			
			if(isset($v1[$y])){
				$message.=    '<li>Code '.$y.': '.$v1[$y].'';  
				$message.="</li>";
				$message.="</br>";
					}
				
				
				
				$y++;
			  } 
		$message.="</ul>";
}


}
		                  $accesscode=json_encode($pArray);
						
						  $orderInvoiceItem =  $objectManager->create('Expert\Invoicegen\Model\Invoicegen');
							
						  $orderInvoiceItem->setData('order_id',$orderid);
						
						  $orderInvoiceItem->setData('accesscode',$accesscode);
	                      $orderInvoiceItem->save();
						  


if(!empty($pArray)){
	
//mail($to,"Code Access$message",$message);
$templateOptions = array('area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $this->storeManager->getStore()->getId());
$templateParams=array('name' =>$customername,'accesscode'=>$message,'order_id'=>$orderid,'customer_name'=>$customername);	
$from = array('email' => "info@examenoverzicht.nl", 'name' => 'ExamenOverzicht.nl');
$this->inlineTranslation->suspend();
$to = $email; 
$transport = $this->_transportBuilder->setTemplateIdentifier('2')
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateParams)
                ->setFrom($from)
                ->addTo($to)
                ->getTransport(); 
$transport->sendMessage();
$this->inlineTranslation->resume();

}


	 }
  
			


	

    
 
}