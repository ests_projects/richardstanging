<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'web2market_wordpress');

/** MySQL database username */
define('DB_USER', 'w2m_wpress');

/** MySQL database password */
define('DB_PASSWORD', '5rgB&&y1Bk2_');

/** MySQL hostname */
define('DB_HOST', '192.168.70.246');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%:4e9t$,zgRc@eHnGtE.+nP:6qX%-e$GInu*:JqJpT@2uX>Rg nvvgo/7?8}v9!@');
define('SECURE_AUTH_KEY',  '5riQ7@ZS$^hDxo3KRhX}:$qy>w5k$9),3mQ5H6y ]a3yRd|Eht`_u*+@>-=^p!23');
define('LOGGED_IN_KEY',    'V?5z1{&Fe@Zr958Ma1:/.ZVtJFPhPJK[?vu<#!>ntP&WrmLkJ[A9|uQOS94~rRnu');
define('NONCE_KEY',        'YD6Kd-,O2LV*2vWuOK]+2n>.0Zc`Vm-zds~6GsiYD5Op#!]3f/Lm*lgtK<YA35SX');
define('AUTH_SALT',        'NU0,z9XC`|4N7#;qdmmd;~6^UC,qERF/wel;(+^oV8F=]lXCfbpP_}0p1*SB3oS.');
define('SECURE_AUTH_SALT', 'MCZ/}I)Mi.J/TG~_}G*3.UK-+0+^V.$>8Ys=RiCf|Saa{Rp}#n%]A1%^y`YgC3p+');
define('LOGGED_IN_SALT',   'W68O99dl2p(V}/IvN&BYW?/|Kgx2WSyZ(nX-cmrR(V5K7x#vimMIbog$duQcGmt:');
define('NONCE_SALT',       '82,dD#+-t;^tBsxF2x[V(G2TkFWy=2;pPO}=jS7y>m+CuayR&YVv>_uk|8^DZdRZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
