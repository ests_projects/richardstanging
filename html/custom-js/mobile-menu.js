// Fire-off right away

// Navigation Slide In
$('.responsive-nav-icon').click(function() {
    $('nav').addClass('slide-in');
    $('html').css("overflow", "hidden");
    $('#overlay').show();
    return false;
});

// Navigation Slide Out
$('#overlay, .responsive-nav-close').click(function() {
    $('nav').removeClass('slide-in');
    $('html').removeAttr("style");
    $('#overlay').hide();
    return false;
});